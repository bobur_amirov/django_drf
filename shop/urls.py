from django.urls import path
from .views import ProductListView, ProductDetialView, ProductCreateView, ProductUpdateView, ProductDeleteView, blog_list, add_blog, test_search

urlpatterns = [
    path('', ProductListView.as_view(), name='product_list'),
    path('product-detial/<int:pk>', ProductDetialView.as_view(), name='product_detial'),
    path('product-create/', ProductCreateView.as_view(), name='product_create'),
    path('product-update/<int:pk>', ProductUpdateView.as_view(), name='product_update'),
    path('product-delete/<int:pk>', ProductDeleteView.as_view(), name='product_delete'),
    path('blog/', blog_list, name='blog_list'),
    path('add/', add_blog, name='add_blog'),
    path('test-search/', test_search, name='test_search'),

]
