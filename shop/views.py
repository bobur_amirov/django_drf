from django.shortcuts import render, redirect
from .models import Product, BlogImg, TestModel
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .forms import BlogImgForm
from .filters import TestFilter
from django.urls import reverse_lazy


class ProductListView(ListView):
    model = Product
    template_name = 'product_list.html'
    context_object_name = 'products'
    ordering = ['-id']


class ProductDetialView(DetailView):
    model = Product
    pk_url_kwarg = 'pk'
    template_name = 'product_detial.html'
    context_object_name = 'product'

class ProductCreateView(CreateView):
    model = Product
    fields = '__all__'
    template_name = 'product_add.html'
    success_url = reverse_lazy("product_list")


class ProductUpdateView(UpdateView):
    model = Product
    fields = '__all__'
    pk_url_kwarg = 'pk'
    template_name = 'product_updete.html'
    success_url = reverse_lazy('product_list')

class ProductDeleteView(DeleteView):
    model = Product
    pk_url_kwarg = 'pk'
    template_name = 'product_delete.html'
    success_url = reverse_lazy('product_list')


def blog_list(request):
    b = BlogImg.objects.all()
    return render(request, 'blog_list.html', {'b': b})


def add_blog(request):
    form = BlogImgForm()
    if request.method == 'POST':
        form = BlogImgForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('blog_list')
            print(form)

    return render(request, 'add.html', {'form': form})


def test_search(request):
    tests = TestModel.objects.order_by('-id')
    test_filter = TestFilter(request.GET, queryset=tests)
    return render(request, 'test.html', {'filter': test_filter})
