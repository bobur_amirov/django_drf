import django_filters
from .models import TestModel


class TestFilter(django_filters.FilterSet):
    class Meta:
        model = TestModel
        fields = {
            'name': ['contains', ],
            'num': ['gte', 'lt'],
            'start_date': ['gte', 'lt']
        }
