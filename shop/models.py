from django.db import models


# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length=100)
    p_type = models.CharField(max_length=100)
    quantity = models.IntegerField()

    def __str__(self):
        return self.name


class BlogImg(models.Model):
    name = models.CharField(max_length=100)
    img = models.ImageField(upload_to='blog/')

    def __str__(self):
        return self.name


class TestModel(models.Model):
    name = models.CharField(max_length=100)
    start_date = models.DateField()
    end_date = models.DateField()
    num = models.IntegerField()
    text = models.TextField()
