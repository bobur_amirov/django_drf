from django.contrib import admin
from .models import Product, BlogImg, TestModel
# Register your models here.

admin.site.register(Product)
admin.site.register(BlogImg)
admin.site.register(TestModel)
