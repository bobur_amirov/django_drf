from django.db import models

# Create your models here

class ApiTest(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField()
    phone = models.IntegerField()
    date = models.DateField()

    def __str__(self):
        return self.title