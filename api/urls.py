from django.urls import path
from .views import ProductAPIView, ProductDetial, ApiTestListView, ApiTestDetial

urlpatterns = [
    path('product/', ProductAPIView.as_view(), name='product_api'),
    path('product/<int:pk>/', ProductDetial.as_view(), name='product_api_detial'),
    path('test/', ApiTestListView.as_view(), name='test_api'),
    path('test/<int:pk>/', ApiTestDetial.as_view(), name='test_api_detial'),

]
