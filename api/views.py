from rest_framework.generics import ListAPIView, RetrieveAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework import permissions
from shop.models import Product
from api.models import ApiTest
from .serializers import ProductSerializer, ApiTestSerializer


class ProductAPIView(ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductDetial(RetrieveAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ApiTestListView(ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated)
    queryset = ApiTest.objects.all()
    serializer_class = ApiTestSerializer


class ApiTestDetial(RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated)
    queryset = ApiTest.objects.all()
    serializer_class = ApiTestSerializer
