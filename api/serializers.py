from rest_framework import serializers

from shop.models import Product
from api.models import ApiTest


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class ApiTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApiTest
        fields = '__all__'
